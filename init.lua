--[[
Charcoal Mod
Version = "1.2"
]]

-- Logging
minetest.log("action", "[MOD] charcoal mod loaded")

-- charcoal:charcoal_lump
minetest.register_craftitem("minetest_charcoal:charcoal_lump", {
  description = "Charcoal",
  inventory_image = "charcoal_lump.png",
})

minetest.register_craft({
  type = "cooking",
  output = "minetest_charcoal:charcoal_lump",
  recipe = "group:tree",
})

minetest.register_craft({
  type = "cooking",
	output = "minetest_charcoal:charcoal_lump",
	recipe = "group:wood",
})

minetest.register_craft({
  type = "fuel",
  recipe = "minetest_charcoal:charcoal_lump",
  burntime = 40,
})

minetest.register_craft({
  output = "default:torch 2",
  recipe = {
    {"minetest_charcoal:charcoal_lump"},
    {"default:stick"},
  }
})

minetest.register_craft({
	type = "shapeless",
	output = "minetest_charcoal:charcoal_lump 9",
	recipe = {"minetest_charcoal:charcoal_block"},
})

-- charcoal:charcoal_block
minetest.register_craftitem("minetest_charcoal:charcoal_block", {
  description = "Charcoal Block",
  inventory_image = "charcoal_block.png"
})

minetest.register_node("minetest_charcoal:charcoal_block", {
  description = "Charcoal Block",
  tiles =  {"charcoal_block.png"},
  is_ground_content = false,
  groups = {cracky = 3, stone = 1},
})

minetest.register_craft({
  output = "minetest_charcoal:charcoal_block",
  recipe = {
    {"minetest_charcoal:charcoal_lump", "minetest_charcoal:charcoal_lump", "minetest_charcoal:charcoal_lump"},
    {"minetest_charcoal:charcoal_lump", "minetest_charcoal:charcoal_lump", "minetest_charcoal:charcoal_lump"},
    {"minetest_charcoal:charcoal_lump", "minetest_charcoal:charcoal_lump", "minetest_charcoal:charcoal_lump"},
  }
})

minetest.register_craft({
	type = "fuel",
	recipe = "minetest_charcoal:charcoal_block",
	burntime = 100,
})

-- black dye recipe
minetest.register_craft({
  output = "dye:black",
  recipe = {
    {"minetest_charcoal:charcoal_lump"},
  }
})
